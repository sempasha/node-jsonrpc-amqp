JSON-RPC пир для RabbitMQ
---

# О проекте

[JSON-RPC](http://www.jsonrpc.org/specification) пир выполняет роль как клиента, который может отправлять запросы 
на выполнение процедур на удалённом сервере (таким же пире), так и сервера, который может принимать запросы на 
выполнение процедур от удалённых клиентов (таких же пиров). Каналом связи при этом случит брокер сообщений RabbitMQ.
Транспорт сообщений между пиром и брокером обеспечивает AMQP-0-9-1 совместимая библиотека 
[amqplib для NodeJS](https://github.com/squaremo/amqp.node). Логика пира отвечающая за 
[JSON-RPC](http://www.jsonrpc.org/specification) основана на библиотеке 
[node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc). Пир занимается только отправкой запросов на выполнение 
процедур и обработкой входящих запросов (и ответов), но не занимается непосредственным выполнением процедур, для 
обеспечения выполнения процедур необходимо подключить контроллер. Для подключения контроллера надо использовать 
встроенные возможности библиотеки [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc) - абстрактный метод
`jsonrpc.Peer#_getRequestedProcedure`.

# Установка

Библиотека доступна на сервисе [Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-amqp). Есть 3 варианта её
загрузки/установки:

 1. Через менеджер пакетов [NPM](#NPM);
 1. Через [Git](#Git);
 1. Загрузка [Zip архива](Zip).

В репозитории для каждой минорной версии библиотеки есть ветка с названием соответствующим этой версии. Название ветки
задаётся в формате `v{major}.{minor}`, где `{major}` - соответствует номеру мажорной версии библиотеки, а `{minor}` -
минорной. (библиотека поддерживает [симантичесую систему определения версии](http://semver.org/)). Так же в главной
ветке `master` репозитория всегда находится самая свежая версия библиотеки. Примеры:

 1. Ветка `v0.1` - содержит библиотеку версии `0.1.x`, где `x` - макисальная версия патча среди пакета версий 0.1.*;
 1. Ветка `v0.5` - содержит библиотеку версии `0.5.x`, `x` - аналогично пункту 1;
 1. `master` - содержит библиотеку самой последней версии.

## NPM

Установка [NPM](https://www.npmjs.org/) пакета последней версии с
[Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-amqp):

```
npm install git+https://bitbucket.org/sempasha/node-jsonrpc-amqp.git --save
```

В этом случае произойдёт установка последней актуальной версии пакета. Для того, чтобы установить пакет определённой
версии - укажите соответствую ветку в адресе:

```
npm install git+https://bitbucket.org/sempasha/node-jsonrpc-amqp.git#v{major}.{minor} --save
```

В качестве альтернативы можно использовать установку из [tarball](http://en.wikipedia.org/wiki/Tarball):

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-amqp/get/master.tar.gz --save
```

или

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-amqp/get/v{minor}.{major}.tar.gz --save
```

## Git

```
git clone git@bitbucket.org:sempasha/node-jsonrpc-amqp.git
```

## Zip

Для загрузки последней версии загрузить [zip мастер ветки](https://bitbucket.org/sempasha/node-jsonrpc-amqp/get/master.zip):

```
https://bitbucket.org/sempasha/node-jsonrpc-amqp/get/master.zip
```

Для загрузки специфической версии:

```
https://bitbucket.org/sempasha/node-jsonrpc-amqp/get/v{minor}.{major}.zip
```

# Тесты

Все компоненты библиотеки покрыты тестами. Запуск тестов:

```
npm run test
```

# API

Документация по API собирается с помощью [JSDoc](http://usejsdoc.org/), для сборки можно использовать NPM run-script

```
npm run docs
```

По умолчанию после сборки документация доступна в папке проекта - [docs](docs/index.html).

Библиотека не поддерживает возможность совершения [Batch](http://www.jsonrpc.org/specification#batch) запросов.